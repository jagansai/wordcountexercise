# Ground Labs Programming Exercise 

Problem Statement The following sequence of commands will work on any POSIX/SUSv3 and will produce the top 20 most frequently used words in the input.    
   
  - (tr "[:space:][:punct:]" "\n" | sort | grep -v "^$" | uniq -c | sort -nr |  head -20) <${INPUT_FILE_NAME} 
 
## Given the book “Moby Dick; Or, The Whale” by Herman Melville, which can be freely obtained by following these links: ● Project Gutenberg book page 
   - https://www.gutenberg.org/ebooks/2701 ● 
   - Direct download, ASCII with CR/LF - 
   - https://www.gutenberg.org/files/2701/2701.txt 
 
 
## To compile the program
   - Download the code.
   - Requirements: 
    - g++ or clang++ compiler with C++ 17 standard support.
	- cmake installed.
   - cd WordCountExercise
   - cmake .
   - make
   
## To run the program
  - ./FrequentWordCount -n 10 -h 2701-0.txt 
  


## Comparison of output between Linux command and the application.

jagansai@DESKTOP-EHC9VFM:~/code/wordcountexercise$ ./FrequentWordCount -n 10 -h /mnt/e/code/Groundlabs/2701-0.txt     
 13868 the   
  6694 of  
  6087 and   
  4617 to   
  4569 a   
  3979 in   
  2882 that   
  2464 his   
  2113 it    
  1846 I   


jagansai@DESKTOP-EHC9VFM:~/code/wordcountexercise$ (tr "[:space:][:punct:]" "\n" | sort | grep -v "^$"  | uniq -c | sort -nr | head -10) < /mnt/e/code/Groundlabs/2701-0.txt    
  13868 the    
   6694 of    
   6087 and    
   4617 to    
   4569 a    
   3979 in    
   2882 that    
   2464 his    
   2113 it    
   1846 I    
jagansai@DESKTOP-EHC9VFM:~/code/wordcountexercise$    

## To get the help on run the application.

   - ./FrequentWordCount --help

