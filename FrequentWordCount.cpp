// FrequentWordCount.cpp : This file contains the 'main' function. Program execution begins and ends there.
// The goal of the program is 
// 1. Read text file(s) as input
// 2. Split the line into words by space and punctuation
// 3. Sort the words by ascending order
// 4. Remove empty lines
// 5. count the words by the occurance
// 6. Display the first n frequent words in descending order

// This programs aims to achieve the functionality of the below Linux command.
// (tr "[:space:][:punct:]" "\n" | sort | grep -v "^$" | uniq -c | sort -nr |  head -20) <${INPUT_FILE_NAME} 
 

#include <iostream>
#include <string>
#include <map>
#include <string_view>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <sstream>
#include <thread>
#include <future>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unordered_map>


typedef std::vector<std::string> Words; // Each line is split into words.

std::string_view punct = "!""#$%&'()*+,-./:;<=>?@[]^_`{|}~"; // [:punct:]
std::string_view space = " \n\t\r"; // [:space:]

namespace ArgUtils {

    // Enum to determine if the output is head or tail of the result.
    enum HeadOrTail : bool {
       head = true,
       tail = false
    };

    // Wrapper type to contain the arguments that are passed to the program.

    struct Args {
        std::vector<std::string> files;
        size_t numberOfWords;
        HeadOrTail headOrTail;
        bool showHelp;

        Args() : files {}, numberOfWords{0}, headOrTail{HeadOrTail::head}, showHelp(false)
        {}

        std::string toString() const {
        std::ostringstream oss;
        oss << "{\n numberOfWords: "
            << numberOfWords << ",\n files: [\n ";
        for (size_t i = 0; i < files.size(); ++i) {
            oss << files[i] << ((i < files.size() - 1) ? ",\n " : "\n");
        }
        oss << " ]\n}";
        return oss.str();
        }    
    };






    // utility function.

    void usage() {
        std::cout << "FrequentWordCount <file1> <file2> <file3> -n <number of words> -[h|t]\n";
    }

    // utility function to parse the arguments.
    Args parseArgs(const int argc, char* argv[]) {
        Args args;
        const std::string_view wordsSwitch = "-n";
        const std::string_view headSwitch = "-h";
        const std::string_view tailSwitch = "-t";
        const std::string_view helpSwitch = "--help";

        for (int i = 1; i < argc;) {
        if (argv[i] == helpSwitch)
        {
           usage();
           args.showHelp = true;
           break;
        }
        if (argv[i] == wordsSwitch && i + 1 < argc) {
            args.numberOfWords = atoi(argv[i + 1]);
            i += 2;
        }
        else if (argv[i] == headSwitch && i + 1 < argc) {
           args.headOrTail = HeadOrTail::head;
           i += 1;
        }
        else if (argv[i] == tailSwitch && i + 1 < argc) {
           args.headOrTail = HeadOrTail::tail;
           i += 1;
        }
        else {
            args.files.push_back(argv[i]);
            ++i;
        }
        }
        return args;
    }
};


namespace Utils {

    Words splitBy(std::string_view file_data, const std::vector<std::string_view>& delimeters) {
        auto cur = file_data.begin();
        auto last = file_data.end();
        auto start = file_data.begin();

        Words words;

        while (true) {
        
        cur = std::find_if(start, last, [&](const char ch) {
            auto it = std::find_if(delimeters.begin(), delimeters.end(), [ch](auto const& str) {
            return str.find_first_of(ch) != std::string::npos;
            });
            return it != delimeters.end();
        });
        words.emplace_back(start, cur);
        if (cur == last) {            
            break;
        }
        ++cur;
        start = cur;
        }
        return words;
    }


    char* map_file(const char* fname, size_t& length)
    {
        int fd = open(fname, O_RDONLY);
        if (fd == -1)
        throw std::runtime_error(std::string {"failed to open file:"} + fname);

        // obtain file size
        struct stat sb;
        if (fstat(fd, &sb) == -1)
        throw std::runtime_error(std::string {"fstat failed on the file:"} + fname);

        length = sb.st_size;

        char* addr = static_cast<char*>(mmap(NULL, length, PROT_READ, MAP_PRIVATE, fd, 0u));
        if (addr == MAP_FAILED)
        throw std::runtime_error( std::string {"Failed to create memory map of the file:"} + fname);

        return addr;
    }

    Words parseWords(const std::string& fileName) {
        Words words;
        size_t length;
        auto f = map_file(fileName.data(), length);
        std::string_view file_data{ f,  length};

        return splitBy(file_data, std::vector<std::string_view>{ punct, space });
    }
};


// Function to show the most common words  or least common words.

template<typename Iterator>
void showWords(Iterator begin, Iterator end, int numberOfWords, ArgUtils::HeadOrTail headOrTail) {

  auto displayHeadPred = [](Iterator it1, Iterator it2) { return std::tie(it1->second, it1->first) > std::tie(it2->second, it2->first); };
  auto displayTailPred = [](Iterator it1, Iterator it2) { return std::tie(it1->second, it1->first) < std::tie(it2->second, it2->first); };



   std::vector<Iterator> wordIters;
   wordIters.reserve(std::distance(begin, end));
   for (auto i = begin; i != end; ++i) wordIters.push_back(i);
   auto sortedRangeEnd = wordIters.begin() + numberOfWords;
   std::partial_sort( wordIters.begin(), sortedRangeEnd, wordIters.end(),
                     [=](Iterator it1, Iterator it2) 
                    { 
                        return (headOrTail == ArgUtils::HeadOrTail::head) ? 
                                displayHeadPred(it1, it2)                 : 
                                displayTailPred(it1, it2);
                    });


   // if output requested is tail and not head, then the above sort gives us the output
   // with words sorted in ascending order.
   // We need the last n frequent words, with words displayed in descending order.
   // we need one more sorting, this time sorting the words with descending order.
   // we only need to sort the filtered list and not the entire collection.

   if (headOrTail == ArgUtils::tail)
   {
       std::sort(wordIters.begin(), sortedRangeEnd, [](Iterator l, Iterator r)
               {
                   return l->first > r->first; 
               });
   }


   for (auto it = wordIters.cbegin(); it != sortedRangeEnd; ++it) {
      std::printf("%6zu %-10s\n", (*it)->second, (*it)->first.c_str());
   } 
}

int main(int argc, char* argv[]) {
    try {
        const ArgUtils::Args args = ArgUtils::parseArgs(argc, argv);
        if (args.files.empty() && !args.showHelp)
        {
           ArgUtils::usage();
           return 0;
        }        
        
        std::vector<std::future<Words>> wordsFuture;
        for (size_t i = 0; i < args.files.size(); ++i) {
            wordsFuture.emplace_back(std::async([fileName = args.files[i]]() {
                return Utils::parseWords(fileName);
            }));
        }
        
        std::unordered_map<std::string, size_t> wordCount;

        for (size_t i = 0; i < wordsFuture.size(); ++i) {
            Words w = wordsFuture[i].get();
            for (auto s : w) {
               if (s == "")
                continue;
               wordCount[s] += 1;
            }
        }
        showWords(wordCount.begin(), wordCount.end(), std::min(args.numberOfWords, wordCount.size()), args.headOrTail);

    }
    catch (std::runtime_error& err) {
        std::cerr << err.what();
    }
}
